const express = require("express");
const app = express();
const productTypeRouter = require("./app/router/productTyperouter");
const productRouter = require("./app/router/productrouter");
const customerRouter = require("./app/router/customerrouter");
const orderRouter = require("./app/router/orderrouter");
const productTypeModel = require("./app/model/productTypeModel");
const productModel = require("./app/model/productModel");
const customerModel = require("./app/model/customerModel");
const orderModel = require("./app/model/orderModel");
const mongoose = require("mongoose");
const port = 8000;

mongoose.connect("mongodb://localhost:27017/shop24h", (err) => {
    if(err){
        throw err
    }
    console.log("connect mongodb successful")
})

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))

app.use("/", productTypeRouter);
app.use("/", productRouter);
app.use("/", customerRouter);
app.use("/", orderRouter);

app.listen(port, () => {
    console.log(`app running at port ${port}`)
})
