const express = require("express");
const {createCustommer, getAllCustommer, getCustommerById, updateCustommer, deleteCustommer} = require("../controllers/customerController");
const router = express.Router();

router.get("/customers", getAllCustommer);
router.get("/customers/:customerid", getCustommerById);
router.put("/customers/:customerid", updateCustommer);
router.post("/customers", createCustommer);
router.delete("/customers/:customerid", deleteCustommer);

module.exports = router;