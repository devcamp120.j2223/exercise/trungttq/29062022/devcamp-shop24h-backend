const express = require("express");
const {createOrderOfCustomer, getAllOrderOfCustomer, getAllOrder, getOrderById, updateOrder, deleteOrder} = require("../controllers/orderController");
const router = express.Router();

router.get("/customers/:customerid/orders", getAllOrderOfCustomer);
router.get("/orders", getAllOrder);
router.get("/orders/:orderid", getOrderById);
router.put("/orders/:orderid", updateOrder);
router.post("/customers/:customerid/orders", createOrderOfCustomer);
router.delete("/customers/:customerid/orders/:orderid", deleteOrder);

module.exports = router;