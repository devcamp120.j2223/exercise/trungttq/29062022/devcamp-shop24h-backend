const express = require("express");
const {createProductType, getAllProDuctType, getProductTypeById, updateProductType, deleteProDuctType} = require("../controllers/productTypeController");
const router = express.Router();

router.get("/producttypes", getAllProDuctType);
router.get("/producttypes/:producttypeid", getProductTypeById);
router.put("/producttypes/:producttypeid", updateProductType);
router.post("/producttypes", createProductType);
router.delete("/producttypes/:producttypeid", deleteProDuctType);

module.exports = router;