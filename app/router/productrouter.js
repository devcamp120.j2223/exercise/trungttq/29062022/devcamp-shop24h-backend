const express = require("express");
const {createProduct, getAllProDuct, getProductById, updateProduct, deleteProDuct} = require("../controllers/productController");
const router = express.Router();

router.get("/products", getAllProDuct);
router.get("/products/:productid", getProductById);
router.put("/products/:productid", updateProduct);
router.post("/products", createProduct);
router.delete("/products/:productid", deleteProDuct);

module.exports = router;