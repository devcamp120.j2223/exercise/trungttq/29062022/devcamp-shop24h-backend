const customerModel = require("../model/customerModel");
const mongoose = require("mongoose");

const createCustommer = (req, res) => {
    let body = req.body;
    if(!body.fullName){
       return res.status(400).json({
            message: `fullName is require`
        })
    }
    if(!body.phone){
        return res.status(400).json({
             message: `phone is require`
         })
     }

     if(!body.email){
        return res.status(400).json({
             message: `email is require`
         })
     }

    else{
        let customer = {
            _id: mongoose.Types.ObjectId(),
            fullName: body.fullName,
            phone: body.phone,
            email: body.email,
            address: body.address,
            city: body.city,
            country: body.country,
            orders: body.orders,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }
        customerModel.create(customer, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    status: "Create customer Success",
                    data: data
                })
            }
        })
    }
     
}

const getAllCustommer = (req, res) => {
    customerModel.find((err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get all customer Success",
                data: data
            })
        }
    })
}

const getCustommerById = (req, res) => {
    let id = req.params.customerid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `customerid id is invalid`
        })
    }else{
        customerModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(200).json({
                    status: "Get customer by ID Success",
                    data: data
                })
            }
        })
    }
}

const updateCustommer = (req, res) => {
    let id = req.params.customerid;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `customerid id is invalid`
        })
    }
    if(!body.fullName){
        return res.status(400).json({
             message: `fullName is require`
         })
     }
     if(!body.phone){
         return res.status(400).json({
              message: `phone is require`
          })
      }
 
      if(!body.email){
         return res.status(400).json({
              message: `email is require`
          })
      }
     
     
     else{
        let customer = {
            
            fullName: body.fullName,
            phone: body.phone,
            email: body.email,
            address: body.address,
            city: body.city,
            country: body.country,
            orders: body.orders,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }

        customerModel.findByIdAndUpdate(id, customer, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(200).json({
                    status: "update customer by ID Success",
                    data: data
                })
            }
        })
     }
}

const deleteCustommer = (req, res) => {
    let id = req.params.customerid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `customerid id is invalid`
        })
    }else{
        customerModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    status: "delete customer by ID Success",
                    data: data
                })
            }
        })
    }
}
module.exports = {createCustommer, getAllCustommer, getCustommerById, updateCustommer, deleteCustommer};