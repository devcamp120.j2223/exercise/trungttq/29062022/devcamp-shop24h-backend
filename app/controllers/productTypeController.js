const productTypeModel = require("../model/productTypeModel");
const mongoose = require("mongoose");

const createProductType = (req, res) => {
    let body = req.body;
    if(!body.name){
       return res.status(400).json({
            message: `name is require`
        })
    }else{
        let productType = {
            _id: mongoose.Types.ObjectId(),
            name: body.name,
            description: body.description,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }
        productTypeModel.create(productType, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    status: "Create productType Success",
                    data: data
                })
            }
        })
    }
     
}

const getAllProDuctType = (req, res) => {
    productTypeModel.find((err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get all productType Success",
                data: data
            })
        }
    })
}

const getProductTypeById = (req, res) => {
    let id = req.params.producttypeid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `producttypeid id is invalid`
        })
    }else{
        productTypeModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(200).json({
                    status: "Get productType by ID Success",
                    data: data
                })
            }
        })
    }
}

const updateProductType = (req, res) => {
    let id = req.params.producttypeid;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `producttypeid id is invalid`
        })
    }
    if(!body.name){
        return res.status(400).json({
             message: `name is require`
         })
     }else{
        let productType = {
           
            name: body.name,
            description: body.description,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }

        productTypeModel.findByIdAndUpdate(id, productType, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(200).json({
                    status: "update productType by ID Success",
                    data: data
                })
            }
        })
     }
}

const deleteProDuctType = (req, res) => {
    let id = req.params.producttypeid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `producttypeid id is invalid`
        })
    }else{
        productTypeModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    status: "delete productType by ID Success",
                    data: data
                })
            }
        })
    }
}
module.exports = {createProductType, getAllProDuctType, getProductTypeById, updateProductType, deleteProDuctType};