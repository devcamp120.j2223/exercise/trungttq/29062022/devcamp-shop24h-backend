const customerModel = require("../model/customerModel");
const orderModel = require("../model/orderModel");
const mongoose = require("mongoose");

const createOrderOfCustomer = (req, res) => {
    let body = req.body;
    let customerid = req.params.customerid;
    if (!mongoose.Types.ObjectId.isValid(customerid)) {
        return res.status(400).json({
            message: `customerid id is invalid`
        })
    } else {
        let order = {
            _id: mongoose.Types.ObjectId(),
            orderDate: body.orderDate,
            shippedDate: body.shippedDate,
            note: body.note,
            orderDetail: body.orderDetail,
            cost: body.cost,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }
        orderModel.create(order, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                customerModel.findByIdAndUpdate(customerid, { $push: { orders: data._id } }, (err, updatedcustomer) => {
                    if (err) {
                        res.status(500).json({
                            message: err.message
                        })
                    } else {
                        res.status(200).json({
                            status: "Create order Success",
                            data: data
                        })
                    }
                })
            }
        })
    }

}

const getAllOrderOfCustomer = (req, res) => {
    let customerid = req.params.customerid;
    if (!mongoose.Types.ObjectId.isValid(customerid)) {
        return res.status(400).json({
            message: `customerid id is invalid`
        })
    } else{
        customerModel.findById(customerid)
        .populate("orders")
        .exec((error, data) => {
            if(error) {
                return res.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return res.status(200).json({
                    status: "Get data success",
                    data: data.orders
                })
            }
        })
    }
    
}

const getAllOrder = (req, res) => {
    orderModel.find((err, data) => {
        if (err) {
            res.status(500).json({
                message: err.message
            })
        } else {
            res.status(200).json({
                status: "Get all order Success",
                data: data
            })
        }
    })
}

const getOrderById = (req, res) => {
    let id = req.params.orderid;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `orderid is invalid`
        })
    } else {
        orderModel.findById(id, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(200).json({
                    status: "Get order by ID Success",
                    data: data
                })
            }
        })
    }
}

const updateOrder = (req, res) => {
    let id = req.params.orderid;
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: `orderid id is invalid`
        })
    }
    


    else {
        let order = {
            
            orderDate: body.orderDate,
            shippedDate: body.shippedDate,
            note: body.note,
            orderDetail: body.orderDetail,
            cost: body.cost,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }

        orderModel.findByIdAndUpdate(id, order, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                res.status(200).json({
                    status: "update order by ID Success",
                    data: data
                })
            }
        })
    }
}

const deleteOrder = (req, res) => {
    let customerid = req.params.customerid;
    let orderid = req.params.orderid;
    if (!mongoose.Types.ObjectId.isValid(customerid)) {
        return res.status(400).json({
            message: `customerid id is invalid`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderid)) {
        return res.status(400).json({
            message: `orderid id is invalid`
        })
    }
    else {
        orderModel.findByIdAndDelete(orderid, (err, data) => {
            if (err) {
                res.status(500).json({
                    message: err.message
                })
            } else {
                customerModel.findByIdAndUpdate(customerid, 
                    {
                        $pull: { orders: orderid } 
                    },
                    (err, updatedCustomer) => {
                        if(err) {
                            return res.status(500).json({
                                status: "Error 500: Internal server error",
                                message: err.message
                            })
                        } else {
                            return res.status(204).json({
                                status: "Success: Delete order success"
                            })
                        }
                    })
            }
        })
    }
}
module.exports = { createOrderOfCustomer, getAllOrderOfCustomer, getAllOrder, getOrderById, updateOrder, deleteOrder };