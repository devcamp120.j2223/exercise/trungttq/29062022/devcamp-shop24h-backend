const productModel = require("../model/productModel");
const mongoose = require("mongoose");

const createProduct = (req, res) => {
    let body = req.body;
    if(!body.name){
       return res.status(400).json({
            message: `name is require`
        })
    }
    if(!body.type){
        return res.status(400).json({
             message: `type is require`
         })
     }
     
     if(!mongoose.Types.ObjectId.isValid(body.type)){
        return res.status(400).json({
             message: `type is invalid`
         })
     }

     if(!body.imageUrl){
        return res.status(400).json({
             message: `imageUrl is require`
         })
     }
     if(!body.buyPrice){
        return res.status(400).json({
             message: `buyPrice is require`
         })
     }
     if(!Number.isInteger(body.buyPrice)){
        return res.status(400).json({
             message: `buyPrice is invalid`
         })
     }
     if(!body.promotionPrice){
        return res.status(400).json({
             message: `promotionPrice is require`
         })
     }
     if(!Number.isInteger(body.promotionPrice)){
        return res.status(400).json({
             message: `promotionPrice is invalid`
         })
     }
     if(body.amount){
        if(!Number.isInteger(body.amount)){
            return res.status(400).json({
                 message: `amount is invalid`
             })
         }
     }
     


    else{
        let product = {
            _id: mongoose.Types.ObjectId(),
            name: body.name,
            description: body.description,
            type: body.type,
            imageUrl: body.imageUrl,
            buyPrice: body.buyPrice,
            promotionPrice: body.promotionPrice,
            amount: body.amount,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }
        productModel.create(product, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(201).json({
                    status: "Create product Success",
                    data: data
                })
            }
        })
    }
     
}

const getAllProDuct = (req, res) => {
    productModel.find((err, data) => {
        if(err){
            res.status(500).json({
                message: err.message
            })
        }else{
            res.status(200).json({
                status: "Get all product Success",
                data: data
            })
        }
    })
}

const getProductById = (req, res) => {
    let id = req.params.productid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `productid id is invalid`
        })
    }else{
        productModel.findById(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(200).json({
                    status: "Get product by ID Success",
                    data: data
                })
            }
        })
    }
}

const updateProduct = (req, res) => {
    let id = req.params.productid;
    let body = req.body;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `productid id is invalid`
        })
    }
    if(!body.name){
        return res.status(400).json({
             message: `name is require`
         })
     }
     if(!body.type){
        return res.status(400).json({
             message: `type is require`
         })
     }
     
     if(!mongoose.Types.ObjectId.isValid(body.type)){
        return res.status(400).json({
             message: `type is invalid`
         })
     }

     if(!body.imageUrl){
        return res.status(400).json({
             message: `imageUrl is require`
         })
     }
     if(!body.buyPrice){
        return res.status(400).json({
             message: `buyPrice is require`
         })
     }
     if(!Number.isInteger(body.buyPrice)){
        return res.status(400).json({
             message: `buyPrice is invalid`
         })
     }
     if(!body.promotionPrice){
        return res.status(400).json({
             message: `promotionPrice is require`
         })
     }
     if(!Number.isInteger(body.promotionPrice)){
        return res.status(400).json({
             message: `promotionPrice is invalid`
         })
     }
     if(body.amount){
        if(!Number.isInteger(body.amount)){
            return res.status(400).json({
                 message: `amount is invalid`
             })
         }
     }
     
     
     else{
        let product = {
            
            name: body.name,
            description: body.description,
            type: body.type,
            imageUrl: body.imageUrl,
            buyPrice: body.buyPrice,
            promotionPrice: body.promotionPrice,
            amount: body.amount,
            timeCreated: body.timeCreated,
            timeUpdated: body.timeUpdated
        }

        productModel.findByIdAndUpdate(id, product, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(200).json({
                    status: "update product by ID Success",
                    data: data
                })
            }
        })
     }
}

const deleteProDuct = (req, res) => {
    let id = req.params.productid;
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: `productid id is invalid`
        })
    }else{
        productModel.findByIdAndDelete(id, (err, data) => {
            if(err){
                res.status(500).json({
                    message: err.message
                })
            }else{
                res.status(204).json({
                    status: "delete product by ID Success",
                    data: data
                })
            }
        })
    }
}
module.exports = {createProduct, getAllProDuct, getProductById, updateProduct, deleteProDuct};