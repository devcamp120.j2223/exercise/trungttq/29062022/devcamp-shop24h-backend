const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productTypeScheme = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String
    },
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("productType", productTypeScheme)