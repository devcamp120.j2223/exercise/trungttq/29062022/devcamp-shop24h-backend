const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productScheme = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String
    },
    type: {
        type: mongoose.Types.ObjectId,
        require: true,
        ref: "productType"
    },
    imageUrl: {
        type: String,
        require: true
    },
    buyPrice: {
        type: Number,
        require: true
    },
    promotionPrice: {
        type: Number,
        require: true
    },
    amount: {
        type: Number,
        default: 0
    },
    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("product", productScheme)