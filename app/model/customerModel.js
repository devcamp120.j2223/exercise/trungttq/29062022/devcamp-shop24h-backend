const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const customerScheme = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    fullName: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true,
        unique: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    address: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    country: {
        type: String,
        default: ""
    },
    orders: [{
        type: mongoose.Types.ObjectId,
        ref: "order"
    }],

    timeCreated: {
        type: Date,
        default: Date.now()
    },
    timeUpdated: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("customer", customerScheme)